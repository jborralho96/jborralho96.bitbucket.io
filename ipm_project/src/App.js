import React, { Component } from 'react';
import './App.css';
import NavBar from './components/NavBar.jsx';
import Filter from './components/Filter/Form.jsx';
import SearchResult from './components/SearchResult/SearchResult.jsx';
import Grid from 'material-ui/Grid';
import films from './server/films.json';


class App extends Component {
  constructor(props) {
   super(props);
   this.state = {
     user: '',
     films: films.films,
     seeLaterList: [],
     isCheck: false,
   };
   this.handleSearch= this.handleSearch.bind(this);
   this.handleSignIn= this.handleSignIn.bind(this);
   this.handleSelectFilm= this.handleSelectFilm.bind(this);
   this.handleSeeLaterList= this.handleSeeLaterList.bind(this);
 }

 checkIfMacth(film, searchFields) {
   if(searchFields.title !== '' && film.title !== searchFields.title) return false;
   if(searchFields.genre !== '' && !film.genre.includes(searchFields.genre)) return false;
   if(searchFields.country !== '' && film.country !== searchFields.country) return false;
   if(searchFields.actor !== '' && !film.actors.includes(searchFields.actor)) return false;
   if(searchFields.director !== '' && film.director !== searchFields.director) return false;
   if(searchFields.mpaa !== '' && film.mpaa !== searchFields.mpaa) return false;
   if(searchFields.minrating !== '' && parseFloat(film.rating) < searchFields.minrating) return false;
   if(searchFields.maxrating !== '' && parseFloat(film.rating) > searchFields.maxrating) return false;
   if(searchFields.minyear !== '' && film.year < searchFields.minyear) return false;
   if(searchFields.maxyear !== '' && film.year > searchFields.maxyear) return false;

   return true;


 }
 handleSearch(searchFields) {
   let searchResult = [];
   const filmsDB = films.films;
   filmsDB.map((film) => {
     if(this.checkIfMacth(film, searchFields)) searchResult.push(film);
   })
   this.setState({
     films: searchResult,
     selectedFilm: '',
   });
 }

 handleSignIn(userName) {
   this.setState({
     user: userName,
   })
 }

 handleSelectFilm(film) {
   this.setState({
     selectedFilm: film,
     isCheck: false,
   })
 }

 handleSeeLaterList(list) {
   this.setState({
     seeLaterList: list,
     isCheck: true,
   })
 }

  render() {
    return (
      <Grid container>
        <Grid item xs={12}>
          <NavBar seeLaterList={this.state.seeLaterList} handleSelectFilm={this.handleSelectFilm} onSignIn={this.handleSignIn} user={this.state.user}/>
        </Grid>

      <Grid container >
        <Grid item xs={8}>
          <SearchResult isCheck={this.state.isCheck} handleSeeLaterList= {this.handleSeeLaterList} user={this.state.user} selectedFilm={this.state.selectedFilm} films={this.state.films}/>
        </Grid>
        <Grid item xs={4} >
          <Filter searchFields={this.handleSearch}/>
        </Grid>
      </Grid>
    </Grid>
    );
  }
}

export default App;
