import React, { Component } from 'react';
import '../../App.css';
import SelectedFilm from './SelectedFilm.jsx';
import ListFilms from './ListFilms.jsx';
import Grid from 'material-ui/Grid';
import films from '../../server/films.json';
import Card from 'material-ui/Card';
import Typography from 'material-ui/Typography';


class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      film: films.films[0],
      listFilms: this.props.films,
    };
    this.SelectFilm = this.SelectFilm.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.selectedFilm) {
      this.setState({
        film: nextProps.selectedFilm,
        listFilms: [nextProps.selectedFilm],
      });
    } else {
      if(!nextProps.isCheck) {
        this.setState({
          film: nextProps.films[0],
          listFilms: nextProps.films,
        });
      }
    }
  }

  SelectFilm(film) {
    this.setState({
      film: film
    });
  }

  render() {
    return (
      this.props.films.length > 0 ?
      <Grid container justify='center'>
        <Grid item xs ={8}>
          <SelectedFilm handleSeeLaterList={this.props.handleSeeLaterList} user={this.props.user} film={this.state.film}/>
        </Grid>
        <Grid item xs={8}>
          <ListFilms films={this.state.listFilms} onClick={this.SelectFilm}/>
        </Grid>
    </Grid>
    :
    <Grid container justify='center'>
        <Typography type="display2" color="inherit" style={{marginTop: '25%'}}>
          Sorry! We couldn't find anything :(
        </Typography>
    </Grid>
    );
  }
}

export default SearchResult;
