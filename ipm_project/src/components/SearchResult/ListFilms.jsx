import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { GridList, GridListTile, GridListTileBar } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorderIcon from 'material-ui-icons/StarBorder';
import Card from 'material-ui/Card';


const styles = theme => ({
  root: {
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  title: {
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    fontWeight: 'bold',
  },
});

class SingleLineGridList extends Component {
  constructor(props) {
   super(props);
   this.state = {
   };
   this.handleClick= this.handleClick.bind(this);
 }
 
 handleClick(film) {
   this.props.onClick(film);
 }
  render() {
    const { classes } = this.props;
      
    return (
      <Card className={classes.root}>
        <GridList id='list' className={classes.gridList} cols={2.5}>
          {this.props.films.map(film => (
            <GridListTile style={{cursor: 'pointer'}} key={film.title} onClick={()=>this.handleClick(film)}>
              <img src={film.img}  alt={''}/>
              <GridListTileBar
                title={film.title}
                classes={{
                  root: classes.titleBar,
                  title: classes.title,
                }}
                actionIcon={
                  <IconButton>
                    <StarBorderIcon className={classes.title} />
                  </IconButton>
                }
              />
            </GridListTile>
          ))}
        </GridList>
      </Card>
    );
  }
}

SingleLineGridList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SingleLineGridList);
