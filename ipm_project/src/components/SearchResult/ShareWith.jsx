/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Grid from 'material-ui/Grid';
import Delete from 'material-ui-icons/Delete';
import Share from 'material-ui-icons/Share';
import Typography from 'material-ui/Typography';
import users from '../../server/users.json';
import List, { ListItem, ListItemText,ListItemSecondaryAction } from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';

const styles = {
  size: {
    height: '100%',
  },
  root: {
    width: '100%',
    maxWidth: 360,
  },
};


class ShareWith extends React.Component {

  constructor(props) {
   super(props);
   let userloggedin='';
   users.users.map((user) => {
     if(user.username === this.props.user)
        userloggedin=user;
   }); //user logged in
   this.state = {
     friends: userloggedin.friends,
     confirm: false,
  };
  this.handleToggle= this.handleToggle.bind(this);
  this.handleConfirm= this.handleConfirm.bind(this);
}

handleToggle(e,friendChecked) {
  let friends = this.state.friends;
  friends.map((friend) => {
    if(friend.username === friendChecked.username) {
      friend.checked = e.target.checked;
    }
  });
  //this.props.onCheck(filmChecked);
   this.setState({
     friends: friends,
   });
 }

 handleConfirm(close) {
   if(close) {
    this.props.handleRequestClose();
  }
   this.setState({
     confirm: !this.state.confirm,
   });
 }
  render() {
    const { classes } = this.props;
    return (
      <div>
          <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()} fullWidth className={classes.size} >
            <DialogContent>
                <Grid container>
                  <Grid item xs={12}>
                    <Typography type="title" color="inherit" align="center">
                      Choose Friend(s)
                    </Typography>
                    <List>
                      {this.state.friends &&
                        this.state.friends.map((friend) => (
                        <ListItem key={friend.username} dense>
                          <ListItemText primary={friend.username}/>
                          <ListItemSecondaryAction>
                            <Checkbox
                              onChange={(e)=>this.handleToggle(e,friend)}
                              checked={friend.checked ? friend.checked : false  }
                            />
                          </ListItemSecondaryAction>
                        </ListItem>
                      ))}
                    </List>
                </Grid>
              </Grid>
            </DialogContent>
            <Grid container justify='center'>
              <Grid item xs={2}>
                <DialogActions>
                      <Button onClick={()=>this.handleConfirm(true)} raised color="default">
                        Share
                        <Share/>
                      </Button>

                </DialogActions>
              </Grid>
            </Grid>
          </Dialog>
            <Dialog open={this.state.confirm} onRequestClose={()=>this.handleConfirm(false)} fullWidth>
              <DialogContent>
                You just shared a movie!
              </DialogContent>
            </Dialog>
          </div>
    );
  }
}


export default withStyles(styles)(ShareWith);
