import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import ShareWith from './ShareWith.jsx';
import users from '../../server/users.json';
import Snackbar from 'material-ui/Snackbar';





const styles = {
  media: {
    maxWidth:'100%',
    maxHeight: '100%',
  },
  card: {
  },

};

class SimpleMediaCard extends Component {
  constructor(props) {
   super(props);
   let isInSeeLater = false;
   let userloggedin='';

   users.users.map((user) => {
     if(user.username === this.props.user)
      userloggedin=user;
   }); //user logged in
   let seeLaterList = [];
   if(userloggedin) {
     console.log(userloggedin);
     seeLaterList = userloggedin.seeLater;

     seeLaterList.map((film) => {
       if(film === this.props.film.title)
        isInSeeLater = true;
     });
   }
   this.state = {
     openShareWith: false,
     isInSeeLater: isInSeeLater,
     seeLaterList: seeLaterList,
     openSnackBar: false,
     removedFromWatchList: false,
   };

  this.handleShareWith = this.handleShareWith.bind(this);
  this.handleToggle = this.handleToggle.bind(this);
  this.handleRequestCloseSnackBar = this.handleRequestCloseSnackBar.bind(this);

 }
 componentWillReceiveProps(nextProps) {
   let isInSeeLater = false;
   let userloggedin='';
   users.users.map((user) => {
     if(user.username === nextProps.user) {
        userloggedin=user;
      }
   }); //user logged in
   let seeLaterList
   if(userloggedin) {
     seeLaterList = userloggedin.seeLater;

     seeLaterList.map((film) => {
       if(film === nextProps.film.title)
        isInSeeLater = true;
     });
   }
   this.setState({
     isInSeeLater: isInSeeLater,
     seeLaterList: seeLaterList,
   });
 }
  handleShareWith() {
    this.setState({
      openShareWith: !this.state.openShareWith,
    })
  }

  handleToggle(e) {
    let seeLaterList = this.state.seeLaterList;
    let removed = false; //removed from watch list
    if(e.target.checked) {
      seeLaterList.push(this.props.film.title);

    } else {
      const index = seeLaterList.indexOf(this.props.film.title);
      seeLaterList.splice(index,1);
      removed = true;
    }
     this.setState({
       isInSeeLater: e.target.checked,
       seeLaterList: seeLaterList,
       openSnackBar:true,
       removedFromWatchList: removed,
     })
     this.props.handleSeeLaterList(seeLaterList);
   }

   handleRequestCloseSnackBar() {
     this.setState({
       openSnackBar:false,
     })
   }

  render() {
    const { classes } = this.props;
    return (
        <Grid container className={classes.card}>
          <Grid item xs={12}>
            <Card>
              <Grid container>
                  <Grid item xs={6}>
                    <img src={this.props.film.img}  alt={this.props.film.title} className={classes.media}/>
                  </Grid>
                  <Grid item xs={6}>
                    <CardContent>
                      <Typography type="headline" component="h2">
                        {this.props.film.title + " (" + this.props.film.year + ")"}
                      </Typography>
                      <Typography component="p">
                            <b>Genre:</b> {this.props.film.genre.join(", ")}
                      </Typography>
                        <Typography component="p">
                            <b>Director:</b> {this.props.film.director}
                        </Typography>
                        <Typography component="p">
                            <b>Country:</b> {this.props.film.country}
                        </Typography>
                        <Typography component="p">
                            <b>Lead Actors:</b> {this.props.film.actors.join(", ")}
                        </Typography>
                        <Typography component="p">
                            <b>MPAA rating:</b> {this.props.film.mpaa}
                        </Typography>
                        <Typography component="p">
                            <b>IMDb rating:</b> {this.props.film.rating}
                        </Typography>
                        <br/>
                    </CardContent>
                    <CardActions>
                      <Grid container>
                        <Grid container>
                          <Grid item xs={6}>
                              <a href={this.props.film.imdb} target="_blank">
                                <Button raised style={{ background: '#f3ce13', color: '#000000'}}>
                                  <b>IMDb</b>
                                </Button>
                              </a>
                            </Grid>
                            { this.props.user &&
                            <Grid item xs ={6} >
                              <Button raised onClick={this.handleShareWith}>
                                Share
                              </Button>
                            </Grid>
                          }
                            </Grid>
                          {this.props.user &&
                            <Grid item xs={12}>
                            <FormControlLabel
                                control={
                                  <Checkbox
                                    onChange={(e)=>this.handleToggle(e)}
                                    label="Watch Later"
                                    checked={this.state.isInSeeLater}
                                  />
                                }
                                label="Watch Later"
                              />
                            </Grid>
                          }
                      </Grid>
                    </CardActions>
                  </Grid>
                </Grid>
            </Card>
            <Snackbar
              anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
              open={this.state.openSnackBar}
              onRequestClose={this.handleRequestCloseSnackBar}
              SnackbarContentProps={{
                'aria-describedby': 'message-id',
              }}
              message={this.state.removedFromWatchList ?
                <span id="message-id">Film removed from watch later list!</span>
                :
                <span id="message-id">Film added to watch later list!</span>
              }
            />
        </Grid>
        {this.props.user &&
          <ShareWith open={this.state.openShareWith} handleRequestClose={this.handleShareWith} user={this.props.user}/>
        }
        </Grid>
    );
  }
}

SimpleMediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleMediaCard);
