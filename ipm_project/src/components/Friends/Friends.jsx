/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Grid from 'material-ui/Grid';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
import Person from 'material-ui-icons/Person';
import Clear from 'material-ui-icons/Clear';
import List, { ListItem, ListItemText,ListItemSecondaryAction } from 'material-ui/List';
import users from '../../server/users.json';


const styles = {
  size: {
    height: '100%',
  },
};

class Friends extends React.Component {

constructor(props) {
   super(props);
   let userLogIn={};
   users.users.map((user)=> {
     if(user.username === this.props.user) {
       userLogIn=user;
     }
   })

  let friends = [];
    
  if(userLogIn.friends) {
   userLogIn.friends.map((friend)=> {
         friends.push(friend);
   })
  }
   this.state = {
      friends: friends,
      user: userLogIn,
  };
    
this.removeFriend = this.removeFriend.bind(this);
  this.setCheckRemove = this.setCheckRemove.bind(this);
}
    
removeFriend(friend)
    {
        let friends = this.state.friends;
        let i = friends.indexOf(friend);
        
        if(i !== -1)
        {
            friends.splice(i, 1);
            this.setState({friends: friends});
        }
    }
    
setCheckRemove(b) {
   this.setState({
     checkRemove: b,
   })
 }


  render() {
    const { classes } = this.props;

    return (
      <Grid container>
        <Grid item xs={12}  >
          <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()} fullWidth className={classes.size} >
            <DialogContent>
                <Grid container>
                  <Grid item xs={12}>
                    <Typography type="title" color="inherit" align="center">
                      Friends
                    </Typography>
                      <List>
                        {this.state.friends.map((friend) => (
                          <ListItem dense button>
                          <Avatar>
                            <Person />
                          </Avatar>
                            <ListItemText primary={friend} />
                          <ListItemSecondaryAction>
                            <Button onClick={()=>{this.removeFriend(friend)}}>
                                <Clear/>
                            </Button>
                          </ListItemSecondaryAction>
                          </ListItem>
                        ))}
                      </List>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
            </DialogActions>
          </Dialog>
      </Grid>
    </Grid>
    );
  }
}


export default withStyles(styles)(Friends);
