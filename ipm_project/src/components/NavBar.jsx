import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Schedule from 'material-ui-icons/Schedule';
import Settings from 'material-ui-icons/Settings';
import Group from 'material-ui-icons/Group';
import SignIn from './Account/SignIn.jsx';
import SignUp from './Account/SignUp.jsx';
import NotSignedIn from './Account/NotSignedIn.jsx';
import SharedAndSeeLater from './SharedAndSeeLater/SharedAndSeeLater.jsx';
import Friends from './Friends/Friends.jsx';

const styles = theme => ({
  root: {
    width: '100%',
  },
  flex: {
    flex: 1,
    textAlign: 'center',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

class NavBar extends Component {
  constructor(props) {
   super(props);
   this.state = {
     openSignIn: false,
     openSignUp: false,
     openSocial: false,
     openFriends: false,
     openNotSignedIn: false,
   };
   this.handleSignIn= this.handleSignIn.bind(this);
   this.handleSignUp= this.handleSignUp.bind(this);
   this.handleNotSignedIn= this.handleNotSignedIn.bind(this);
   this.handleSocial= this.handleSocial.bind(this);
   this.handleFriends= this.handleFriends.bind(this);
 }

 handleSignIn(){
   this.setState ({ openSignIn: !this.state.openSignIn });
 }
 handleSignUp(){
   this.setState ({ openSignUp: !this.state.openSignUp });
 }
 handleNotSignedIn(){
   this.setState ({ openNotSignedIn: !this.state.openNotSignedIn });
 }
 handleSocial(){
     if (this.props.user)
        this.setState ({ openSocial: !this.state.openSocial });
     else
         this.handleNotSignedIn();
 }
 handleFriends(){
     if (this.props.user)
        this.setState ({ openFriends: !this.state.openFriends });
     else
         this.handleNotSignedIn();
 }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar style={{backgroundColor: 'black'}} position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="contrast" aria-label="Menu" onClick={this.handleSocial}>
              <Schedule />
            </IconButton>
            <IconButton className={classes.menuButton} color="contrast" aria-label="Menu" onClick={this.handleFriends}>
              <Group />
            </IconButton>
            <Typography type="title" color="inherit" className={classes.flex}>
              PulpCorn
            </Typography>
            {!this.props.user &&
                <div>
                    <Button onClick={this.handleSignIn} color="contrast">Log in</Button>
                    <Button onClick={this.handleSignUp} color="contrast">Sign up</Button>
                </div>
            }
            {this.props.user &&
              <div>
                <Typography type="title" color="inherit" className={classes.flex}>
                    {this.props.user}
                </Typography>
                <Button onClick={() => this.props.onSignIn("")} color="contrast">Log out</Button>
                {/*<IconButton className={classes.menuButton} color="contrast" aria-label="Menu">
                    <Settings />
                </IconButton>*/}
             </div>
            }
          </Toolbar>
        </AppBar>
        <SignIn title={'Log In'} onSignIn={this.props.onSignIn} open={this.state.openSignIn} handleRequestClose={this.handleSignIn}/>
        <SignUp title={'Register'} onSignUp={this.props.onSignIn} open={this.state.openSignUp} handleRequestClose={this.handleSignUp}/>
        <NotSignedIn open={this.state.openNotSignedIn} handleRequestClose={this.handleNotSignedIn}/>
       {this.props.user &&
          <SharedAndSeeLater
          seeLaterList={this.props.seeLaterList}
          handleSelectFilm={this.props.handleSelectFilm}
          user={this.props.user}
          open={this.state.openSocial}
          handleRequestClose={this.handleSocial}/>
                }
            {this.props.user &&
                    <Friends
                        user = {this.props.user}
                        open={this.state.openFriends}
                        handleRequestClose={this.handleFriends}
                        />
            }
      </div>
    );
  }
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);
