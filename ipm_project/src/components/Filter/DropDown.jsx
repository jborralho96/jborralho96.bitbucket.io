import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const genre = ['Action','Adventure','Animation','Biography','Comedy','Crime','Documentary','Drama',
               'Family','Fantasy','Film-Noir','History','Horror','Music','Musical','Mystery','Romance',
               'Sci-Fi','Sport','Thriller','War','Western']

const country= ['Afghanistan', 'Åland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bangladesh', 'Barbados', 'Bahamas', 'Bahrain', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'British Virgin Islands', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burma', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo-Brazzaville', 'Congo-Kinshasa', 'Cook Islands', 'Costa Rica', 'Croatia', 'Curaçao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'El Salvador', 'Egypt', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Federated States of Micronesia', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Lands', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Heard and McDonald Islands', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kekistan', 'Kenya', 'Kiribati', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Islands', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Réunion', 'Romania', 'Russia', 'Rwanda', 'Saint Barthélemy', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre and Miquelon', 'Saint Vincent', 'Samoa', 'San Marino', 'São Tomé and Príncipe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia', 'South Korea', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Sweden', 'Swaziland', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'UK', 'USA', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City', 'Vietnam', 'Venezuela', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe']

const mpaa= ['G','NC-17','PG','PG-13','R']

const rating = ['1','2','3','4','5','6','7','8','9','10']

const year = ['1900','1910','1920','1930','1940','1950','1960','1970','1980','1990','2000','2010','2020']

class SimpleSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ selected: event.target.value });
  };

  render() {
    const { classes } = this.props;
    return (
      <form className={classes.container} autoComplete="off">
        <FormControl className={classes.formControl}>
          {this.props.type === 'genre' &&
          <InputLabel htmlFor="age-simple">Genre</InputLabel>
          }
          {this.props.type === 'country' &&
          <InputLabel htmlFor="age-simple">Country</InputLabel>
          }
          {this.props.type === 'mpaa' &&
          <InputLabel htmlFor="age-simple">MPAA</InputLabel>
          }
          {this.props.type === 'maxrating' &&
          <InputLabel htmlFor="age-simple">Max rating</InputLabel>
          }
          {this.props.type === 'minrating' &&
          <InputLabel htmlFor="age-simple">Min rating</InputLabel>
          }
          {this.props.type === 'minyear' &&
          <InputLabel htmlFor="age-simple">Min year</InputLabel>
          }
          {this.props.type === 'maxyear' &&
          <InputLabel htmlFor="age-simple">Max year</InputLabel>
          }
          <Select
            value={this.state.selected}
            onChange={(e)=>{
              this.handleChange(e);
              this.props.onChange(e);
            }}
            input={<Input id="age-simple" />}
          >
            <MenuItem value="">
              None
            </MenuItem>
            {this.props.type === 'genre' &&
            genre.map((genre) =>
                <MenuItem key={genre} value={genre}>{genre}</MenuItem>
              )
            }
            {this.props.type === 'country' &&
            country.map((country) =>
              <MenuItem key={country} value={country}>{country}</MenuItem>)
            }
            {this.props.type === 'mpaa' &&
            mpaa.map((mpaa) =>
              <MenuItem key={mpaa} value={mpaa}>{mpaa}</MenuItem>)
            }
            {this.props.type === 'minrating' &&
            rating.map((minrating) =>
              <MenuItem key={minrating} value={minrating}>{minrating}</MenuItem>)
            }
            {this.props.type === 'maxrating' &&
            rating.map((maxrating) =>
              <MenuItem key={maxrating} value={maxrating}>{maxrating}</MenuItem>)
            }
            {this.props.type === 'minyear' &&
            year.map((minyear) =>
              <MenuItem key={minyear} value={minyear}>{minyear}</MenuItem>)
            }
            {this.props.type === 'maxyear' &&
            year.map((maxyear) =>
              <MenuItem key={maxyear} value={maxyear}>{maxyear}</MenuItem>)
            }

          </Select>
        </FormControl>

      </form>
    );
  }
}

SimpleSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
};

export default withStyles(styles)(SimpleSelect);
