import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import DropDown from './DropDown.jsx';
import Button from 'material-ui/Button';
import Search from 'material-ui-icons/Search';
import Clear from 'material-ui-icons/Clear';
import { FormLabel, FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';

const styles = theme => ({
  root: {
  },
});

class AutoGrid extends React.Component {

constructor(props) {
  super(props);
  this.state = {
    title: '',
    genre: '',
    country: '',
    actor: '',
    director: '',
    mpaa: '',
    keyword: '',
    minrating: '',
    maxrating: '',
    minyear: '',
    maxyear: '',
    invalid: false,
    empty: true,
    clear: false
  };
  this.handleSearch = this.handleSearch.bind(this);
  this.handleClear = this.handleClear.bind(this);
  this.handleInvalid = this.handleInvalid.bind(this);
  this.markEmpty = this.markEmpty.bind(this);
}

handleSearch() {
    if (!this.state.invalid)
    {
    const searchFields= {
    title: this.state.title,
    genre: this.state.genre,
    country: this.state.country,
    actor: this.state.actor,
    director: this.state.director,
    mpaa: this.state.mpaa,
    keyword: this.state.keyword,
    minrating: this.state.minrating,
    maxrating: this.state.maxrating,
    minyear: this.state.minyear,
    maxyear: this.state.maxyear,
    invalid: false
  }
  this.props.searchFields(searchFields);
    window.scroll(0, 0);
    }
}

handleClear()
{
  const clear0 = !this.state.clear
  this.setState({
    title: '',
    genre: '',
    country: '',
    actor: '',
    director: '',
    mpaa: '',
    keyword: '',
    minrating: '',
    maxrating: '',
    minyear: '',
    maxyear: '',
      empty: true,
    invalid: false,
    clear: clear0
  });
    window.scroll(0, 0);
}

handleInvalid(b)
{
    console.log(b)
    this.setState({invalid: b})
}

markEmpty(e)
    {
        const b = e.target.value == "" ? true : false;
        this.setState({empty: b});
    }

  render() {
      return (
            <Grid container key = {this.state.clear}>
              <Grid item xs={12}>
                    <TextField
                      label="Title"
                      margin="normal"
                      onChange={(e) =>{this.setState ({ title: e.target.value }); this.markEmpty(e)}}
                    />
              </Grid>

              <Grid item xs={6}>
                  <DropDown
                    onChange={(e) =>{this.setState ({ genre: e.target.value }); this.markEmpty(e)}}
                    type='genre'
                  />
              </Grid>
              <Grid item xs={6}>
                  <DropDown
                    onChange={(e) =>{this.setState ({ country: e.target.value }); this.markEmpty(e)}}
                    type='country'
                  />
              </Grid>
              <Grid item xs={6}>
                  <TextField
                    id="actor"
                    label="Actor"
                    margin="normal"
                    onChange={(e) =>{this.setState ({ actor: e.target.value }); this.markEmpty(e)}}
                  />
              </Grid>
              <Grid item xs={6}>
                  <TextField
                    id="director"
                    label="Director"
                    margin="normal"
                    onChange={(e) =>{this.setState ({ director: e.target.value }); this.markEmpty(e)}}
                  />
              </Grid>
              <Grid item xs={12}>
                  <DropDown
                    onChange={(e) =>{this.setState ({ mpaa: e.target.value }); this.markEmpty(e)}}
                    type='mpaa'
                  />
              </Grid>
              <Grid item xs={6}>
                <DropDown
                  onChange={
                          (e) => {
                                    this.setState ({ minrating: e.target.value });
                                if (this.state.maxrating !== "" && e.target.value > this.state.maxrating)
                                    this.handleInvalid(true)
                                else
                                    this.handleInvalid(false);
                              this.markEmpty(e)
                          }
                          }
                    type='minrating'/>
              </Grid>
              <Grid item xs={6}>
                <DropDown
                  onChange={
                          (e) => {
                                    this.setState ({ maxrating: e.target.value });
                                    console.log('minrating',this.state.minrating)
                                    console.log('value',e.target.value)
                                if (this.state.minrating !== '' && parseInt(e.target.value) < this.state.minrating){
                                  this.handleInvalid(true)
                                }
                                else {
                                  this.handleInvalid(false);
                                }
                              this.markEmpty(e)
                                }
                      }
                    type='maxrating'/>
              </Grid>
              <Grid item xs={6}>
                <DropDown
                  onChange={
                          (e) => {
                                    this.setState ({ minyear: e.target.value });
                                if (this.state.maxyear !== "" && e.target.value > this.state.maxyear)
                                    this.handleInvalid(true)
                                else
                                    this.handleInvalid(false);
                              this.markEmpty(e)
                                }
                      }
                  type='minyear'/>
              </Grid>
              <Grid item xs={6}>
                <DropDown
                  onChange={
                          (e) => {
                                    this.setState ({ maxyear: e.target.value })
                                if (this.state.minyear !== "" && e.target.value < this.state.minyear)
                                    this.handleInvalid(true)
                                else
                                    this.handleInvalid(false);
                              this.markEmpty(e)
                                }
                      }
                  type='maxyear'/>
              </Grid>
              {this.state.invalid &&
              <Grid item >
                        <FormControl component="fieldset" required error>
              <FormHelperText> Valores inválidos! </FormHelperText>
                      </FormControl>
              </Grid>
                  }
            <Grid item xs={12}/>
              <Grid item>
                <Button onClick={this.handleSearch} raised color="default">
                  Search
                  <Search className={this.props.classes.rightIcon} />
                </Button>
              </Grid>
            {!this.state.empty &&
              <Grid item>
                <Button onClick={this.handleClear} raised color="default">
                  Clear
                <Clear className={this.props.classes.rightIcon} />
                </Button>
              </Grid>
                  }
            </Grid>
        );
    }
  }

AutoGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AutoGrid);
