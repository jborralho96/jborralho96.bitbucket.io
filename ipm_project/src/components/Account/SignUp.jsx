/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import users from '../../server/users.json';
import { FormLabel, FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';



const usersDir='../../server/users.json';
export default class FormDialog extends React.Component {

  constructor(props) {
   super(props);
   this.state = {
     userName: '',
     pwd: '',
     confirmPwd: '',
   };
   this.handleConfirm= this.handleConfirm.bind(this);
 }

 handleConfirm() {
   const username= this.state.userName;
   const pwd= this.state.pwd;
   const confirmPwd= this.state.confirmPwd;
   let isInDB = false;
   users.users.map((user) => {
     if(user.username === username) {
       isInDB = true;
     }
   })
   if(username === '' || pwd === '' || confirmPwd === '' || pwd !== confirmPwd || isInDB) {
     this.setState({
       msgErr: true,
     })
   } else {
     this.props.handleRequestClose();
     this.props.onSignUp(username);
   }

 }

  render() {
    return (
      <div>
        <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()}>
          <DialogTitle>{this.props.title}</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="User name"
              type="email"
              fullWidth
              onChange={(e)=> this.setState ({ userName: e.target.value })}
                onKeyPress={(ev) =>
                           {
              if (ev.key === 'Enter')
                  {
                      this.handleConfirm();
                      ev.preventDefault();
                  }
          }
                           }
            />
            <TextField
              margin="dense"
              id="name"
              label="Password"
              type="password"
              fullWidth
              onChange={(e)=> this.setState ({ pwd: e.target.value })}
                onKeyPress={(ev) =>
                           {
              if (ev.key === 'Enter')
                  {
                      this.handleConfirm();
                      ev.preventDefault();
                  }
          }
                           }

            />
            <TextField
              margin="dense"
              id="name"
              label="Confirm Password"
              type="password"
              fullWidth
              onChange={(e)=> this.setState ({ confirmPwd: e.target.value })}
                onKeyPress={(ev) =>
                           {
              if (ev.key === 'Enter')
                  {
                      this.handleConfirm();
                      ev.preventDefault();
                  }
          }
                           }

            />
            {this.state.msgErr &&
            <FormControl component="fieldset" required error>
              <FormHelperText>Something went wrong. Please check if the pwds match or try other user name</FormHelperText>
            </FormControl>
          }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleRequestClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleConfirm} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
