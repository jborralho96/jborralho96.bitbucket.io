import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogTitle,
} from 'material-ui/Dialog';

export default class FormDialog extends React.Component {

  constructor(props) {
   super(props);
 }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()}>
          <DialogTitle>You need to be loggeded in to use that!</DialogTitle>
          <DialogActions>
            <Button onClick={()=>this.props.handleRequestClose()} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
