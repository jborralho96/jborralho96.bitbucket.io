/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import { FormLabel, FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';

import users from '../../server/users.json';

export default class FormDialog extends React.Component {

  constructor(props) {
   super(props);
   this.state = {
     userName: '',
     pwd:'',
     wrong: false, //to show message of wrong username or pass
   };
   this.handleConfirm= this.handleConfirm.bind(this);

 }

 handleConfirm() {
   const usersDB=users.users;
   console.log(users.users);
   if(!usersDB.some((e)=>{
     return (e.username == this.state.userName && e.pwd == this.state.pwd);
   })) {
     this.setState({
       wrong: true,
     })
   } else {
     this.props.handleRequestClose();
     this.props.onSignIn(this.state.userName);
   }
   //saveAs(file);
 }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()}>
          <DialogTitle>{this.props.title}</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="User name"
              type="email"
              onChange={(e)=> this.setState ({ userName: e.target.value })}
              fullWidth
                onKeyPress={(ev) =>
                           {
              if (ev.key === 'Enter')
                  {
                      this.handleConfirm();
                      ev.preventDefault();
                  }
          }
                           }
            />
            <TextField
              margin="dense"
              id="name"
              label="Password"
              type="password"
              onChange={(e)=> this.setState ({ pwd: e.target.value })}
              fullWidth
                onKeyPress={(ev) =>
                           {
              if (ev.key === 'Enter')
                  {
                      this.handleConfirm();
                      ev.preventDefault();
                  }
          }
                           }
            />
            {this.state.wrong &&
            <FormControl component="fieldset" required error>
              <FormHelperText>The user name or the password are not correct.</FormHelperText>
            </FormControl>
          }
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>this.props.handleRequestClose()} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleConfirm} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
