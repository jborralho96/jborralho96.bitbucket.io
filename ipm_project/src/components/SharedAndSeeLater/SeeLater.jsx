import React from 'react';
import Button from 'material-ui/Button';
import Avatar from 'material-ui/Avatar';
import List, { ListItem, ListItemText,ListItemSecondaryAction } from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';



const styles = {
  size: {
    height: '100%',
  },
  root: {
    width: '100%',
    maxWidth: 360,
  },
};
export default class SeeLater extends React.Component {

  constructor(props) {
     super(props);

     this.state = {
      user: this.props.user,
      filmsSeeLater: this.props.filmsSeeLater,
    };
    this.handleToggle= this.handleToggle.bind(this);
  }

  componentWillReceiveProps(nextProps) {
      this.setState({
        filmsSeeLater: nextProps.filmsSeeLater,
      });
  }

  handleToggle(e,filmChecked) {
    let filmsSeeLater = this.state.filmsSeeLater;
    filmsSeeLater.map((film) => {
      if(film.title === filmChecked.title) {
        film.checked = e.target.checked;
      }
    });
    this.props.onCheck(filmChecked);
     this.setState({
       filmsSeeLater: filmsSeeLater,
     });
   }

   handleClickFilm(film) {
     this.props.handleSelectFilm(film);
     this.props.handleRequestClose();
   }

  render() {
    const { classes } = this.props;
    return (
      <List>
        {this.state.filmsSeeLater.map((film) => (
          <ListItem key={film.title} dense button onClick={() =>this.handleClickFilm(film)}>
            <Avatar alt={film.title} src={film.img} />
            <ListItemText primary={film.title} />
            <ListItemSecondaryAction>
              <Checkbox
                onChange={(e)=>this.handleToggle(e,film)}
                checked={film.checked ? film.checked : false  }
              />
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    );
  }
}
