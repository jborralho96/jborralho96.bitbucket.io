import React from 'react';
import Button from 'material-ui/Button';
import Avatar from 'material-ui/Avatar';
import List, { ListItem, ListItemText,ListItemSecondaryAction } from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import ArrowForward from 'material-ui-icons/ArrowForward';




const styles = {
  size: {
    height: '100%',
  },
  root: {
    width: '100%',
    maxWidth: 360,
  },
};
export default class Shared extends React.Component {

  constructor(props) {
     super(props);

     this.state = {
      user: this.props.user,
      filmsRecommended: this.props.filmsRecommended,
    };
    this.handleToggle= this.handleToggle.bind(this);
  }

  componentWillReceiveProps(nextProps) {
      this.setState({
        filmsRecommended: nextProps.filmsRecommended,
      });
  }

  handleToggle(e,filmChecked) {
    let filmsRecommended = this.state.filmsRecommended;
    filmsRecommended.map((film) => {
      if(film.title === filmChecked.title) {
        film.checked = e.target.checked;
      }
    });
    this.props.onCheck(filmChecked);
     this.setState({
       filmsRecommended: filmsRecommended,
     });
   }

   handleClickFilm(film) {
     this.props.handleSelectFilm(film);
     this.props.handleRequestClose();
   }

  render() {
    const { classes } = this.props;
    return (
      <List>
        {this.state.filmsRecommended.map((film) => (
          <ListItem key={film.title} dense button onClick={() =>this.handleClickFilm(film)}>
            <Avatar alt={film.title} src={film.img} />
            <ListItemText primary={film.title} secondary={"shared by test1"} />
            <ListItemSecondaryAction>
              <Checkbox
                onChange={(e)=>this.handleToggle(e,film)}
                checked={film.checked ? film.checked : false  }
              />
            </ListItemSecondaryAction>
          </ListItem>
        ))}
        {this.state.filmsRecommended.length > 0 &&
          <Button onClick={this.props.handleWatchLater} raised color="default">
            Watch Later
            <ArrowForward/>
          </Button>
      }
      </List>
    );
  }
}
