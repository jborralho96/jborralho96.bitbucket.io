/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Grid from 'material-ui/Grid';
import Delete from 'material-ui-icons/Delete';
import Typography from 'material-ui/Typography';
import Shared from './Shared.jsx';
import SeeLater from './SeeLater.jsx';
import users from '../../server/users.json';
import films from '../../server/films.json';




const styles = {
  size: {
    height: '100%',
  },
  root: {
    width: '100%',
    maxWidth: 360,
  },
};


class SharedAndSeeLAter extends React.Component {

  constructor(props) {
   super(props);
   let userLogIn={};
   users.users.map((user)=> {
     if(user.username === this.props.user) {
       userLogIn=user;
     }
   })
   let filmsRecommended = [];
   let filmsSeeLater = [];
   if(userLogIn.recommended) {
     userLogIn.recommended.map((filmTitle)=> {
       films.films.map((film) => {
         if(film.title === filmTitle) {
           filmsRecommended.push(film);
         }
       });
     })
   }
  if(userLogIn.seeLater) {
   userLogIn.seeLater.map((filmTitle)=> {
     films.films.map((film) => {
       if(film.title === filmTitle) {
         filmsSeeLater.push(film);
       }
     });
   })
  }
   this.state = {
      filmsRecommended: filmsRecommended,
      filmsSeeLater: filmsSeeLater,
      recommendedChecked: [],
      seeLaterChecked: [],
      user: userLogIn,
      checkRemove: false,
  };
  this.onCheck = this.onCheck.bind(this);
  this.deleteSelected = this.deleteSelected.bind(this);
  this.checkRemove = this.checkRemove.bind(this);
  this.handleRequestCloseConfirm = this.handleRequestCloseConfirm.bind(this);
  this.handleWatchLater = this.handleWatchLater.bind(this);

 }

 componentWillReceiveProps(nextProps) {
   let filmsSeeLater = [];
   this.state.user.seeLater.map((filmTitle)=> {
      films.films.map((film) => {
        if(film.title === filmTitle) {
          filmsSeeLater.push(film);
        }
      });
    })

    this.setState({
      filmsSeeLater: filmsSeeLater
    })
 }
 handleWatchLater() {
   let recommendedChecked = this.state.recommendedChecked;
   let seeLaterChecked = this.state.seeLaterChecked;

   let recommended = this.state.filmsRecommended;
   let seeLater = this.state.filmsSeeLater;

   recommendedChecked.map((film) => {
     const i = recommended.indexOf(film);
     recommended.splice(i, 1);
   });

   seeLaterChecked.map((film) =>{
     const i = seeLater.indexOf(film);
      if(i === -1) {
        film.checked = false;
      	seeLater.push(film);
      }
   });
   this.setState({
     filmsRecommended: recommended,
     filmsSeeLater: seeLater,
     recommendedChecked: [],
     seeLaterChecked: [],
   })
 }

 handleRequestCloseConfirm() {
   this.setState({
     checkRemove: false,
   })
 }
 onCheck(film) {
   let recommendedChecked = this.state.recommendedChecked;
   let i = recommendedChecked.indexOf(film);
    if(i !== -1) {
    	recommendedChecked.splice(i, 1);
    } else {
      recommendedChecked.push(film);
    }

    let seeLaterChecked = this.state.seeLaterChecked;
    i = seeLaterChecked.indexOf(film);
     if(i !== -1) {
     	seeLaterChecked.splice(i, 1);
     } else {
       seeLaterChecked.push(film);
     }
   this.setState({
     recommendedChecked,
     seeLaterChecked,
   })
 }

 checkRemove() {
   this.setState({
     checkRemove: true,
   })
 }

 deleteSelected() {
   let recommendedChecked = this.state.recommendedChecked;
   let seeLaterChecked = this.state.seeLaterChecked;
   let recommended = this.state.filmsRecommended;
   let seeLater = this.state.filmsSeeLater;

   recommendedChecked.map((film) => {
     const i = recommended.indexOf(film);
      if(i !== -1) {
      	recommended.splice(i, 1);
      }
   });

   seeLaterChecked.map((film) =>{
     const i = seeLater.indexOf(film);
      if(i !== -1) {
      	seeLater.splice(i, 1);
      }
   });
   this.handleRequestCloseConfirm();
   this.setState({
     filmsRecommended: recommended,
     filmsSeeLater: seeLater,
     recommendedChecked: [],
     seeLaterChecked: [],
   })

 }

  render() {
    const { classes } = this.props;

    return (
      <Grid container>
        <Grid item xs={12}  >
          <Dialog open={this.props.open} onRequestClose={()=>this.props.handleRequestClose()} fullWidth className={classes.size} >
            <DialogContent>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography type="title" color="inherit" align="center">
                      Recommended
                    </Typography>
                  <Shared
                    handleSelectFilm={this.props.handleSelectFilm}
                    filmsRecommended={this.state.filmsRecommended}
                    user={this.state.user}
                    handleRequestClose={this.props.handleRequestClose}
                    handleWatchLater={this.handleWatchLater}
                    onCheck={this.onCheck}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography type="title" color="inherit" align="center">
                    Watch Later
                  </Typography>
                  <SeeLater
                    handleSelectFilm={this.props.handleSelectFilm}
                    filmsSeeLater={this.state.filmsSeeLater}
                    user={this.state.user}
                    handleRequestClose={this.props.handleRequestClose}
                    onCheck={this.onCheck}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <Grid container justify='center'>
              <Grid item xs={2}>
                <DialogActions>
                      <Button onClick={this.checkRemove} raised color="default">
                        <Delete className={this.props.classes.rightIcon} />
                      </Button>

                </DialogActions>
              </Grid>
            </Grid>
          </Dialog>
          <Dialog open={this.state.checkRemove} onRequestClose={()=>this.handleRequestCloseConfirm()} fullWidth>
            <DialogContent>
              Are you sure you wnat to remove the selected films from the list(s)?
            </DialogContent>
            <DialogActions>
              <Button onClick={this.deleteSelected} raised color="default">
                Yes
              </Button>
              <Button onClick={this.handleRequestCloseConfirm} raised color="default">
                No
              </Button>
            </DialogActions>
          </Dialog>
      </Grid>
    </Grid>
    );
  }
}


export default withStyles(styles)(SharedAndSeeLAter);
