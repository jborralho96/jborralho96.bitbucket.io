var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

 module.exports = {
    entry: './src/index.js',
    output: {
       path: path.resolve(__dirname, 'build'),
       filename: 'app.bundle.js'
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },

    module: {
       loaders: [
           {
               test: /\.(js|jsx)$/,
               loader: 'babel-loader',
               query: {
                     presets: ['es2015', 'react']
                 },
                exclude: [/node_modules/]
           },
           {
             test: /\.css$/,
             loader: "style-loader!css-loader"
           }
       ]
    },
    plugins: [
      new HtmlWebpackPlugin()
    ]
 };
